package and.halal.halalcheckedapi.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceIndisponibleException extends Exception {

	public ServiceIndisponibleException(String string) {
		
		super(string);
	}

}

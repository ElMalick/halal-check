package and.halal.halalcheckedapi.utilil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Cette classe répertorie les additifs non halals et ceux douteux ainsi que les traitement qui vont avec
 * @author elhadjimalickthiam
 *
 */
public class Additifs {

	
	/* Paramétre de onnexion à la base de données */
	static final String url = "jdbc:mysql://localhost:8889/halal_checker_api?serverTimezone = EST5EDT";
	static final String utilisateur = "malick";
	static final String motDePasse = "360adn";
	public static Connection connexion = null;


	public static ResultSet getIngredients() {
		
	    try {
	    	
	      Class.forName( "com.mysql.cj.jdbc.Driver" );
	      //System.out.println("Driver O.K.");  
	    } catch (Exception e) {
	    	
	    	//System.out.println("Driver non ok");
	    	e.printStackTrace();
	    } 
	        
	    connexion = null;
	    try {
	        connexion = DriverManager.getConnection( url, utilisateur, motDePasse );
	        //System.out.println("la connection O.K.");
	        
	    } catch ( SQLException e ) {
	    	System.out.println("connection non O.K.");
	    	e.printStackTrace();
	    } 
		
		ResultSet resultat=null;  
		Statement statement = null;
	    try {
	    	statement  = connexion.createStatement();
	    	resultat = statement.executeQuery( "SELECT *  FROM HaramIngredients;" );  
	    } catch (Exception e) { 	
	    	System.out.println("echec de la requéte");
	    	e.printStackTrace();
	    } 
	    return resultat;
	}
	
	
	public static ResultSet getAdditifs(String type) {
		
		ResultSet resultat=null;  
	     
	    /* Création de l'objet gérant les requêtes */
	    Statement statement = null;
	    try {
	    	statement  = connexion.createStatement();
	    	resultat = statement.executeQuery( "SELECT *  FROM " + type + "Additifs;" );  
	    } catch (Exception e) { 	
	    	System.out.println("echec de la requéte");
	    	e.printStackTrace();
	    } 
	      		
		return resultat;
	}
	
	
	
	/**
	 * Vérifie si un additif est non halal
	 * @param Une faisantl'objet de la vérification
	 * @param la liste des additifs non halal
	 * @return	un Additif
	 */
	public static Additif haram (String str, String p1,String p2) {
		
		Additif add = new Additif(true,false,null);
		str.toLowerCase();
		
		ResultSet haramIngredients = getIngredients(); 
		try {
			while ( haramIngredients.next() )
				if (str.contains(haramIngredients.getString( "nom" ).toLowerCase()))
					{
						add.setHalal(false);
						String[] tab = {"présence de "+haramIngredients.getString( "nom" ) + " détectée", null, null};
						add.setStr(tab);
						return add;
					} 
			}catch ( SQLException e ) {
				e.printStackTrace();
			} 
		
		ResultSet haramAdditis = getAdditifs("Haram"); 
		
		try {
			while ( haramAdditis.next() )
				if (str.contains(haramAdditis.getString( "id" ).toLowerCase()) && !haramAdditis.getString( "id" ).toLowerCase().equals(p1.toLowerCase()) && !haramAdditis.getString( "id" ).toLowerCase().equals(p2.toLowerCase()) )
					{
						add.setHalal(false);
						String[] tab = {haramAdditis.getString( "id" ),haramAdditis.getString( "nom" ),haramAdditis.getString( "description" )};
						add.setStr(tab);
						return add;
					} 
			}catch ( SQLException e ) {
				e.printStackTrace();
			} 
		   
		return add;
	}
	
	/**
	 * Vérifie si un additif est douteux
	 * @param Une faisantl'objet de la vérification
	 * @param la liste des additifs douteux
	 * @return	un Additif
	 */
	public static Additif douteux(String str, String p1,String p2) {
		
		Additif add = new Additif(true,false,null);
		str.toLowerCase();
		
		ResultSet douteuxAdditis = getAdditifs("Douteux"); 
		
		try {
			while ( douteuxAdditis.next() )
				if (str.contains(douteuxAdditis.getString( "id" ).toLowerCase()) && !douteuxAdditis.getString( "id" ).toLowerCase().equals(p1.toLowerCase()) && !douteuxAdditis.getString( "id" ).toLowerCase().equals(p2.toLowerCase()) )
					{
						add.setDoute(true);
						String[] tab = {douteuxAdditis.getString( "id" ),douteuxAdditis.getString( "nom" ),douteuxAdditis.getString( "description" )};
						add.setStr(tab);
						return add;
					} 
			}catch ( SQLException e ) {
				e.printStackTrace();
			} 
		
		try {
		    connexion.close();
		   // System.out.println("connection non fermé");
	   }catch ( SQLException e ) {
		    	System.out.println("connection non fermé");
		    	e.printStackTrace();
	   } 
		
		return add;
	}
}

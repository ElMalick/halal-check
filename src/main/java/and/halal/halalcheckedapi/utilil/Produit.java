package and.halal.halalcheckedapi.utilil;

public class Produit {

	private String made_in;
	private String ingredients;
	private String haram_ingredients;
	private String additive_id;
	private String additive_name;
	private String additive_description;
	private String image_url;
	
	public Produit() {
		
	}

	public Produit(String made_in, String ingredients, String haram_ingredients, String additive_id,
			String additive_name, String additive_description, String image_url) {
		super();
		this.made_in = made_in;
		this.ingredients = ingredients;
		this.haram_ingredients = haram_ingredients;
		this.additive_id = additive_id;
		this.additive_name = additive_name;
		this.additive_description = additive_description;
		this.image_url = image_url;
	}

	public String getMade_in() {
		return made_in;
	}

	public void setMade_in(String made_in) {
		this.made_in = made_in;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getHaram_ingredients() {
		return haram_ingredients;
	}

	public void setHaram_ingredients(String haram_ingredients) {
		this.haram_ingredients = haram_ingredients;
	}

	public String getAdditive_id() {
		return additive_id;
	}

	public void setAdditive_id(String additive_id) {
		this.additive_id = additive_id;
	}

	public String getAdditive_name() {
		return additive_name;
	}

	public void setAdditive_name(String additive_name) {
		this.additive_name = additive_name;
	}

	public String getAdditive_description() {
		return additive_description;
	}

	public void setAdditive_description(String additive_description) {
		this.additive_description = additive_description;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	
}
